"use strict";

function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

(function () {
  // Fetch data from destinations.json
  fetch("../destinations.json").then(function (response) {
    return response.json();
  }).then(function (json) {
    var destinations = _toConsumableArray(json.destinations);

    initCard(destinations);
  });
})();

var initCard = function initCard(destinations) {
  var container = document.querySelector(".container-card");
  var cardTemplate = document.createElement("template");
  var index = 0; // Loop through data destinations and create card for each destination

  var _iterator = _createForOfIteratorHelper(destinations),
      _step;

  try {
    var _loop = function _loop() {
      var d = _step.value;
      var rating = d.rating,
          img = d.img;
      var id = index++;
      var count = 0;
      var rate = parseInt(rating); // Create Card template with data

      cardTemplate.innerHTML = createCardTemplate(d, id);
      container.appendChild(cardTemplate.content); // Create as many stars from rating

      var containerRating = createContainerStars(rate); // Select card poster container

      var containerPoster = document.querySelector(".poster-".concat(id)); // Create poster container

      var poster = document.createElement("div");
      poster.classList.add("container-poster"); // Check if we need to create a slider

      if (!Array.isArray(img)) {
        poster.innerHTML = "<img src=\"../images/".concat(img, "\" alt=\"").concat(img, "\"></img>");
      } else {
        // Create all img tag from img array
        var imgTemplate = createImgTemplate(img); // Create slider container

        var slider = document.createElement("div");
        slider.classList.add("slider-posters"); // Append all img tag on slider container

        slider.appendChild(imgTemplate.content);
        poster.appendChild(slider); // Create Button Slider

        var prevSlide = createPreviousButtonSlider(poster, count);
        var nextSlide = createNextButtonSlider(poster, count); // Add click event on button slider

        prevSlide.addEventListener("click", function () {
          var prevCount = count;
          count <= 0 ? count = img.length - 1 : count--;
          onChangeSlide(prevCount, count, slider);
        });
        nextSlide.addEventListener("click", function () {
          var prevCount = count;
          count < img.length - 1 ? count++ : count = 0;
          onChangeSlide(prevCount, count, slider);
        });
      }

      containerPoster.appendChild(poster); // Create div upto

      var containerUpto = createContainerUpto(d.upto);
      poster.appendChild(containerUpto); // Append Container rating

      var containerInfosLabel = document.querySelector(".label-".concat(id));
      containerInfosLabel.appendChild(containerRating);
    };

    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      _loop();
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }
};

var createCardTemplate = function createCardTemplate(destination, id) {
  var country = destination.country,
      place = destination.place,
      label = destination.label,
      tags = destination.tags;
  return "\n  <div class=\"card\">\n    <div class=\"card-poster poster-".concat(id, "\">\n    </div>\n    <div class=\"card-body\">\n      <div class=\"card-body-infos\">\n        <div class=\"card-body-element \">\n          <span class=\"country\">").concat(country, "</span>\n          <span> - </span>\n          <span class=\"place\">").concat(place, "</span>\n        </div>\n        <div class=\"card-body-infos-label card-body-element label-").concat(id, "\">\n          <span class=\"label\">").concat(label, "</span>\n        </div>\n        <div class=\"card-body-infos-tags card-body-element\">\n          <div class=\"tag tag-premium\">").concat(tags[0].label, "</div>\n          <div class=\"tag tag-option\">").concat(tags[1].label, "</div>\n        </div>\n      </div>\n      <div class=\"card-body-button\">\n        <button class=\"btnRedirect\">></button>\n      </div>\n    </div>\n  </div>\n   ");
};

var createContainerStars = function createContainerStars(rate) {
  var star = "<svg height=\"12px\" viewBox=\"0 -10 511.99143 511\" width=\"12px\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"m510.652344 185.882812c-3.371094-10.367187-12.566406-17.707031-23.402344-18.6875l-147.796875-13.417968-58.410156-136.75c-4.3125-10.046875-14.125-16.53125-25.046875-16.53125s-20.738282 6.484375-25.023438 16.53125l-58.410156 136.75-147.820312 13.417968c-10.835938 1-20.011719 8.339844-23.402344 18.6875-3.371094 10.367188-.257813 21.738282 7.9375 28.925782l111.722656 97.964844-32.941406 145.085937c-2.410156 10.667969 1.730468 21.699219 10.582031 28.097656 4.757813 3.457031 10.347656 5.183594 15.957031 5.183594 4.820313 0 9.644532-1.28125 13.953125-3.859375l127.445313-76.203125 127.421875 76.203125c9.347656 5.585938 21.101562 5.074219 29.933593-1.324219 8.851563-6.398437 12.992188-17.429687 10.582032-28.097656l-32.941406-145.085937 111.722656-97.964844c8.191406-7.1875 11.308594-18.535156 7.9375-28.925782zm-252.203125 223.722657\"/></svg>";
  var container = document.createElement("div");

  for (var i = 0; i < rate; i++) {
    var span = document.createElement("span");
    span.innerHTML = star;
    container.appendChild(span);
  }

  return container;
};

var createImgTemplate = function createImgTemplate(arr) {
  var length = arr.length;
  var imgContent = "";
  var template = document.createElement("template");

  for (var i = 0; i < length; i++) {
    if (i === 0) imgContent += "<img src=\"../images/".concat(arr[i], "\" alt=\"").concat(arr[i], "\"></img>");else imgContent += "<img class=\"slider-inactive\" src=\"../images/".concat(arr[i], "\" alt=\"").concat(arr[i], "\"></img>");
  }

  template.innerHTML = imgContent;
  return template;
};

var createPosterToSlide = function createPosterToSlide(rate) {
  var star = "<svg height=\"12px\" viewBox=\"0 -10 511.99143 511\" width=\"12px\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"m510.652344 185.882812c-3.371094-10.367187-12.566406-17.707031-23.402344-18.6875l-147.796875-13.417968-58.410156-136.75c-4.3125-10.046875-14.125-16.53125-25.046875-16.53125s-20.738282 6.484375-25.023438 16.53125l-58.410156 136.75-147.820312 13.417968c-10.835938 1-20.011719 8.339844-23.402344 18.6875-3.371094 10.367188-.257813 21.738282 7.9375 28.925782l111.722656 97.964844-32.941406 145.085937c-2.410156 10.667969 1.730468 21.699219 10.582031 28.097656 4.757813 3.457031 10.347656 5.183594 15.957031 5.183594 4.820313 0 9.644532-1.28125 13.953125-3.859375l127.445313-76.203125 127.421875 76.203125c9.347656 5.585938 21.101562 5.074219 29.933593-1.324219 8.851563-6.398437 12.992188-17.429687 10.582032-28.097656l-32.941406-145.085937 111.722656-97.964844c8.191406-7.1875 11.308594-18.535156 7.9375-28.925782zm-252.203125 223.722657\"/></svg>";
  var container = document.createElement("div");

  for (var i = 0; i < rate; i++) {
    var span = document.createElement("span");
    span.innerHTML = star;
    container.appendChild(span);
  }

  return container;
};

var createPreviousButtonSlider = function createPreviousButtonSlider(poster) {
  var prevSlide = document.createElement("div");
  prevSlide.innerHTML = "<";
  prevSlide.classList.add("previous-slide");
  poster.appendChild(prevSlide);
  return prevSlide;
};

var createNextButtonSlider = function createNextButtonSlider(poster) {
  var nextSlide = document.createElement("div");
  nextSlide.innerHTML = ">";
  nextSlide.classList.add("next-slide");
  poster.appendChild(nextSlide);
  return nextSlide;
};

var onChangeSlide = function onChangeSlide(prevCount, count, slider) {
  slider.children[prevCount].classList.remove("slider-active");
  slider.children[prevCount].classList.add("slider-inactive");
  slider.children[count].classList.remove("slider-inactive");
  slider.children[count].classList.add("slider-active");
};

var createContainerUpto = function createContainerUpto(upto) {
  var container = document.createElement("div");
  container.classList.add("upto-element");
  container.innerHTML = upto;
  return container;
};