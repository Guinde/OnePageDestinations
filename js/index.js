(function () {
  // Fetch data from destinations.json
  fetch("../destinations.json")
    .then(response => response.json())
    .then(json => {
      const destinations = [...json.destinations];
      initCard(destinations);
    });
})();

const initCard = destinations => {
  const container = document.querySelector(".container-card");
  const cardTemplate = document.createElement("template");

  let index = 0;

  // Loop through data destinations and create card for each destination
  for (const d of destinations) {
    const { rating, img } = d;
    const id = index++;
    let count = 0;
    const rate = parseInt(rating);

    // Create Card template with data
    cardTemplate.innerHTML = createCardTemplate(d, id);
    container.appendChild(cardTemplate.content);

    // Create as many stars from rating
    const containerRating = createContainerStars(rate);

    // Select card poster container
    const containerPoster = document.querySelector(`.poster-${id}`);

    // Create poster container
    const poster = document.createElement("div");
    poster.classList.add("container-poster");

    // Check if we need to create a slider
    if (!Array.isArray(img)) {
      poster.innerHTML = `<img src="../images/${img}" alt="${img}"></img>`;
    } else {
      // Create all img tag from img array
      const imgTemplate = createImgTemplate(img);

      // Create slider container
      const slider = document.createElement("div");
      slider.classList.add("slider-posters");

      // Append all img tag on slider container
      slider.appendChild(imgTemplate.content);
      poster.appendChild(slider);

      // Create Button Slider
      const prevSlide = createPreviousButtonSlider(poster, count);
      const nextSlide = createNextButtonSlider(poster, count);

      // Add click event on button slider
      prevSlide.addEventListener("click", () => {
        let prevCount = count;
        count <= 0 ? (count = img.length - 1) : count--;
        onChangeSlide(prevCount, count, slider);
      });
      nextSlide.addEventListener("click", () => {
        let prevCount = count;
        count < img.length - 1 ? count++ : (count = 0);
        onChangeSlide(prevCount, count, slider);
      });
    }
    containerPoster.appendChild(poster);

    // Create div upto
    const containerUpto = createContainerUpto(d.upto);
    poster.appendChild(containerUpto);

    // Append Container rating
    const containerInfosLabel = document.querySelector(`.label-${id}`);
    containerInfosLabel.appendChild(containerRating);
  }
};

const createCardTemplate = (destination, id) => {
  const { country, place, label, tags } = destination;
  return `
  <div class="card">
    <div class="card-poster poster-${id}">
    </div>
    <div class="card-body">
      <div class="card-body-infos">
        <div class="card-body-element ">
          <span class="country">${country}</span>
          <span> - </span>
          <span class="place">${place}</span>
        </div>
        <div class="card-body-infos-label card-body-element label-${id}">
          <span class="label">${label}</span>
        </div>
        <div class="card-body-infos-tags card-body-element">
          <div class="tag tag-premium">${tags[0].label}</div>
          <div class="tag tag-option">${tags[1].label}</div>
        </div>
      </div>
      <div class="card-body-button">
        <button class="btnRedirect">></button>
      </div>
    </div>
  </div>
   `;
};

const createContainerStars = rate => {
  const star = `<svg height="12px" viewBox="0 -10 511.99143 511" width="12px" xmlns="http://www.w3.org/2000/svg"><path d="m510.652344 185.882812c-3.371094-10.367187-12.566406-17.707031-23.402344-18.6875l-147.796875-13.417968-58.410156-136.75c-4.3125-10.046875-14.125-16.53125-25.046875-16.53125s-20.738282 6.484375-25.023438 16.53125l-58.410156 136.75-147.820312 13.417968c-10.835938 1-20.011719 8.339844-23.402344 18.6875-3.371094 10.367188-.257813 21.738282 7.9375 28.925782l111.722656 97.964844-32.941406 145.085937c-2.410156 10.667969 1.730468 21.699219 10.582031 28.097656 4.757813 3.457031 10.347656 5.183594 15.957031 5.183594 4.820313 0 9.644532-1.28125 13.953125-3.859375l127.445313-76.203125 127.421875 76.203125c9.347656 5.585938 21.101562 5.074219 29.933593-1.324219 8.851563-6.398437 12.992188-17.429687 10.582032-28.097656l-32.941406-145.085937 111.722656-97.964844c8.191406-7.1875 11.308594-18.535156 7.9375-28.925782zm-252.203125 223.722657"/></svg>`;
  const container = document.createElement("div");
  for (let i = 0; i < rate; i++) {
    const span = document.createElement("span");
    span.innerHTML = star;
    container.appendChild(span);
  }
  return container;
};

const createImgTemplate = arr => {
  const length = arr.length;
  let imgContent = "";
  const template = document.createElement("template");
  for (let i = 0; i < length; i++) {
    if (i === 0)
      imgContent += `<img src="../images/${arr[i]}" alt="${arr[i]}"></img>`;
    else
      imgContent += `<img class="slider-inactive" src="../images/${arr[i]}" alt="${arr[i]}"></img>`;
  }
  template.innerHTML = imgContent;
  return template;
};

const createPosterToSlide = rate => {
  const star = `<svg height="12px" viewBox="0 -10 511.99143 511" width="12px" xmlns="http://www.w3.org/2000/svg"><path d="m510.652344 185.882812c-3.371094-10.367187-12.566406-17.707031-23.402344-18.6875l-147.796875-13.417968-58.410156-136.75c-4.3125-10.046875-14.125-16.53125-25.046875-16.53125s-20.738282 6.484375-25.023438 16.53125l-58.410156 136.75-147.820312 13.417968c-10.835938 1-20.011719 8.339844-23.402344 18.6875-3.371094 10.367188-.257813 21.738282 7.9375 28.925782l111.722656 97.964844-32.941406 145.085937c-2.410156 10.667969 1.730468 21.699219 10.582031 28.097656 4.757813 3.457031 10.347656 5.183594 15.957031 5.183594 4.820313 0 9.644532-1.28125 13.953125-3.859375l127.445313-76.203125 127.421875 76.203125c9.347656 5.585938 21.101562 5.074219 29.933593-1.324219 8.851563-6.398437 12.992188-17.429687 10.582032-28.097656l-32.941406-145.085937 111.722656-97.964844c8.191406-7.1875 11.308594-18.535156 7.9375-28.925782zm-252.203125 223.722657"/></svg>`;
  const container = document.createElement("div");
  for (let i = 0; i < rate; i++) {
    const span = document.createElement("span");
    span.innerHTML = star;
    container.appendChild(span);
  }
  return container;
};

const createPreviousButtonSlider = poster => {
  const prevSlide = document.createElement("div");
  prevSlide.innerHTML = "<";
  prevSlide.classList.add("previous-slide");
  poster.appendChild(prevSlide);
  return prevSlide;
};

const createNextButtonSlider = poster => {
  const nextSlide = document.createElement("div");
  nextSlide.innerHTML = ">";
  nextSlide.classList.add("next-slide");
  poster.appendChild(nextSlide);
  return nextSlide;
};

const onChangeSlide = (prevCount, count, slider) => {
  slider.children[prevCount].classList.remove("slider-active");
  slider.children[prevCount].classList.add("slider-inactive");

  slider.children[count].classList.remove("slider-inactive");
  slider.children[count].classList.add("slider-active");
};

const createContainerUpto = upto => {
  const container = document.createElement("div");
  container.classList.add("upto-element");
  container.innerHTML = upto;
  return container;
};
